Flexperto - Final Application Test
=====================================

THE TASK
--------------

Your task is to build an User-CRUD. You will find an already prepared User model in the common tier, including an authentication life-cycle (sign-up/sign-in/password-reset). What we expect:

Create your local YII 2 environment based on instruction below.

1.  add an Avatar to Users
2.  add a mobile number to Users
3.  build a profile for registered Users (including regular show, editing and canceling the account)


You can use whatever IDE / component / technology you would like to, as long as this application stays the HTML-generating, server-side component. Keep in mind that your code will have to run in our Apache. An addition to this file for special setup instructions, such for example bower and stuff, would be wise if needed :-)


### The Hook


We really want you to show us your strengths. That's why we decided to design this task in a very open ended manner.

This means that we give you a guideline for 3 hours to solve the general task. How much time you actually spend is totally up to you. It can be less or twice as much.


### What to do with the time


Well, first of all you should try to deliver the key task. You are free to set your focus on model || controllers || views if you feel to. If you think that one or several items of the general task are for example generally outdated and bad practice, you can note this at the end of this section of the README, just skip it and get the task done in less than 5 hours. If you feel that there should be more to it, or you want to show us some nifty skills, or js libraries, or whatever - feel free to raise your time budget. You get the idea, right? Get the task done - in a way that you feel comfortable with.

When you are finished and feel the need to, you can also state specific things you focused on here:


### How to proceed

Fork this repository on bitbucket. When you are finished, share the repository with Caspar Bauer:

mail: caspar.bauer@flexperto.com

skype: designtesbrot



### Questions ?

Feel free to contact Caspar Bauer:

mail: caspar.bauer@flexperto.com

skype: designtesbrot

available at any time


APPLICATION INTRODUCTION
------------------------ 

This Application is based on the [Yii 2 Advanced Application Template](https://github.com/yiisoft/yii2-app-advanced).

Yii 2 Advanced Application Template is a skeleton Yii 2 application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.


DIRECTORY STRUCTURE
-------------------

```
app/common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
app/console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
app/backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
app/frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
app/vendor/              contains dependent 3rd-party packages
app/environments/        contains environment-based overrides
app/tests                contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```


INSTALLATION
------------

If you do not have [Docker](https://www.docker.com/), you may install it by following the instructions
at [docker.com](https://www.docker.com/products/overview).

`cd` into this folder and run `docker-compose up`. This will build and start the yii2 application next to a mysql instance.
Most things are allready configured. What's left for you to do is:
 
1. Set the hostnames

in your /etc/hosts file create these entries:

```
127.0.0.1 frontend.flexperto.dev
127.0.0.1 backend.flexperto.dev
```

You should now be able to browse the applications [frontend](http://frontend.flexperto.dev:8000/) and [backend](http://backend.flexperto.dev:8000/)

2. Configure composer with your github oauth token

In order to Install the required vendor libraries via composer, you will need to handover your [github oauth token to composer](https://getcomposer.org/doc/articles/troubleshooting.md#api-rate-limit-and-oauth-tokens)
When you obtained your oAuth token, use this command to configure composer:

`docker exec flexpertofat_flexperto_1 composer config -g github-oauth.github.com YOU_TOKEN`

3. Install composer libraries

Next you need to install the vendor libraries through composer. To do so, run:

`docker exec flexpertofat_flexperto_1 composer install --dev`

4. Run the migrations

You will need to create your database scheme. The application comes with migrations ready to use. In order to run them, execute the following command:

`docker exec flexpertofat_flexperto_1 php yii migrate --interactive=0`